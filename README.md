# whadru-cookbook

A basic set of recipes for configuring, deploying, and running WhadRULookingAt?

## Supported Platforms

Ubuntu 14.04

## Usage

(No recipes yet!)

## Authors

Author:: Andrew Pawloski (<apawloski+e84@gmail.com>)
