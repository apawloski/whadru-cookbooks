#!/usr/bin/env bats

@test "docker is found in PATH" {
  run which docker
  [ "$status" -eq 0 ]
}