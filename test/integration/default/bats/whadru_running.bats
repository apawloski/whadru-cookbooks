#!/usr/bin/env bats

# Do we get an HTTP OK status from the app?
@test "WhadRU is running" {
  result=$(curl -I localhost:4567 2>/dev/null | head -n 1 | cut -d$' ' -f2)
  [ "$result" -eq 200 ]
}