#
# Cookbook Name:: whadru-cookbook
# Recipe:: webapp
#
# This recipe results in a running WhadRULookingAt? Web Application

# What version of the docker container do we want?
image="#{node[:dockerhub_user]}/#{node[:app_image]}"
version="#{node[:app_release]}"

# We want to install docker if it's not already there.
#
# Note: This recipe is a workaround for an OpsWorks bug with docker cookbook
# Once Chef 11.16 is offered on OpsWorks, revert to the docker default recipe
include_recipe "whadru-cookbook::install_docker"

docker_image "#{image}" do
  tag version
  action :pull
end

# Run the container (attaching to host)
docker_container "#{image}" do
  detach true
  # NOTE: This gives the container the host's network interface
  # If we ever want a model with multiple containers, this needs to change
  net "host" 
end