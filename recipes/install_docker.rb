# This is a temporary workaround for https://github.com/bflad/chef-docker/issues/215
#
# Using this recipe only because Chef 11.10 has an error with apt/gpg signing 
# while trying to install docker.
# 
# Issue 215 has been resolved with later versions of Chef. Once OpsWorks upgrades,
# we can go back to docker::default and download from the ubuntu repo.

# Update our repositories
#
# Not officially supporting fedora/centos
# Just giving an example of how we'd handle different platforms
case node[:platform]
when "ubuntu","debian"
  include_recipe "apt"
when "centos,fedora"
  include recipe "yum"
end

package "curl" do
  action :install
end

bash "Install docker manually" do
  code "curl -Ssl get.docker.com | sh"
  not_if "which docker" # Is docker already there?
end
