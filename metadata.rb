name "whadru-cookbook"
maintainer "Andrew Pawloski"
maintainer_email "apawloski@gmail.com"
description "A cookbook for preparing for and running WhadRULookingAt?"
version "1.0"

# What other cookbooks do we need?
depends "docker"

# Only tested on ubuntu
supports "ubuntu", "~> 14.04"

# Basic recipe declarations go here
recipe "whadru::webapp", "Installs and runs the WhadRULookingAt? WebApp container"
